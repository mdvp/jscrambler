var chai = require('chai');
chai.use(require('chai-as-promised'));
var expect = chai.expect;
var jscrambler = require('./dist/jscrambler');

var simpleHTML = '<p style="color:red;">coiso</p>';
var osmttscom = 'http://osmtts.com';
var origami = 'http://origamitemmagia.pt';

describe('jscrambler', function() {
	it('Simple HTML:' + simpleHTML, function() {
		var js = new jscrambler.Jscrambler();

		js.setHtml(simpleHTML);
		
		return js.getInfoAsync()
			.then((json) => {
				//expect(json.resources).to.be.empty();
				expect(json.tags_counter[0].name).to.equal('p');
			});
	});

	it('osmtts.com', function() {
		var js = new jscrambler.Jscrambler();

		js.setUrl(osmttscom);
		
		return js.getInfoAsync()
			.then((json) => {
				expect(json.tags_counter[0].name).to.equal('html');
			});
	});

	it('origami web page', function() {
		var js = new jscrambler.Jscrambler();

		js.setUrl(origami);
		
		return js.getInfoAsync()
			.then((json) => {
				expect(json.resources).to.have.lengthOf(1); 
			});
	});
});