var gulp = require('gulp');
var babel = require('gulp-babel');
var minify = require('gulp-minify');
var clean = require('gulp-clean');

gulp.task('clean', function () {
  return gulp.src('dist', {read: false})
    .pipe(clean());
});

gulp.task('es6', ['clean'], function() {
	return gulp.src('src/*.js')
	.pipe(babel({ presets: ['es2015'] }))
	.pipe(gulp.dest('dist'));
});

/*
gulp.task('libs', function(){
	return gulp.src([
		'node_modules/systemjs/dist/system.js',
		'node_modules/babel-polyfill/dist/polyfill.js'])
	.pipe(gulp.dest('dist/libs'));
});
*/

gulp.task('dev', ['es6']);

gulp.task('default', ['es6'], function(){
	return gulp.src('dist/*.js') 
		.pipe(minify())
		.pipe(gulp.dest('dist'));
});
