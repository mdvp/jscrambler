# README #

Pretende-se que seja implementada uma CLI (Command Line Interface) em Node que
receba como input um URL para um recurso HTML remoto e que devolva como output
um JSON com meta informação relevante sobre a markup obtida.

Exemplos de meta informação pertinente:

* Contagem de tags HTML (por nome);
* Contagem de atributos por tag HTML;
* Tipos de recursos descarregados (imagem, vídeo, etc) e hosts de onde foram
  descarregados;
* Número de filhos e quais, por tipo de tag HTML;
* A profundidade da árvore;

Toda a meta informação adicional será considerada e valorizada.

### Requisitos ###

- Desenvolver a solução em ES6;
- Produção de testes unitários;
- Produção de documentação.

### Entrega ###

A entrega deve ser feita, enviando um arquivo `tar.gz` que contenha o
repositório bem como qualquer outro elemento produzido e relevante para a
avaliação da solução entregue.

### Comandos ###

*dev* compila o código para a pasta *dist*.
*build* compila e minifica. 
*bbuild* compila sem utilizar o Gulp.
*builddoc* compila e gera a documentação.
```
#!cli

npm run dev
npm run build
npm run bbuild
npm run builddoc
```

Testar código com um endereço especifico ou html.
Se não forem especificados parâmetros serão executados os testes unitários do ficheiro *test.js*.
```
#!cli

npm run test:url http://endereco.pt
npm run test:http "<p>teste</p>"
npm run test
```

Utilizando Node:
```
#!cli

node index.js -u http://endereco.pt
node index.js -url http://endereco.pt
node index.js -s "<p>teste</p>"
node index.js -html "<p>teste</p>"
```