'use strict';

const validUrl = require('valid-url');
const urlapi = require('url');
const cheerio = require('cheerio');

var _URL;
var _HTML;

function _getContentAsync (url) {
	return new Promise((resolve, reject) => {
		const lib = url.startsWith('https') ? require('https') : require('http');
		const request = lib.get(url, (response) => {
			if (response.statusCode < 200 || response.statusCode > 299) {
				reject(new Error('Failed to load page, status code: ' + response.statusCode));
			}

			const body = [];
			response.on('data', (chunk) => body.push(chunk));
			response.on('end', () => resolve(body.join('')));
		});
		request.on('error', (err) => reject(err))
	});
}

function _inspectHtml (rootElm, tagWorker) {
	function _inspectAsync (elm, depth) {
		return new Promise((resolve) => {
			if (Array.isArray(elm)) {
				while (elm.length > 0) {
					_inspectAsync(elm.shift(), depth + 1);	
				}

				resolve();
			}
			else {
				if (elm.type == 'style' || elm.type == 'tag' || elm.type == 'script') {
					tagWorker(elm, depth);
				}

				if (elm.children) {
					_inspectAsync(elm.children, depth);
				}
				else {
					resolve();	
				}
			}
		});
	}

	_inspectAsync(rootElm, -2); //-2 because rootElm has other non-element info produced on cheerio
}

function _trySetTagAsync (arr, elm) {
	return new Promise((resolve) => {
		var idx = arr.findIndex(x => x.tag == elm.name);

		if (idx >= 0) {
			arr[idx].count++;
		}
		else {
			arr.push({
				'tag': elm.name,
				'count': 1
			});
		}

		resolve();
	});
}

function _trySetAttribsAsync (arr, elm) {
	return new Promise((resolve) => {
		var idx = arr.findIndex(x => x.tag == elm.name);
		var c = Object.keys(elm.attribs).length;

		if (idx >= 0) {
			arr[idx].count += c;
		}
		else {
			arr.push({
				'tag': elm.name,
				'count': c
			});
		}

		resolve();
	});
}

function _trySetRsxsAsync (arr, elm) {
	return new Promise((resolve) => {
		var obj = { };

		if (elm.name == 'img') {
			obj.type = 'image';
			obj.src = elm.attribs.src;
		}
		else if (elm.name == 'video') {
			obj.type = 'video';
			obj.src = elm.attribs.src;
		}
		else if (elm.name.includes('media')) {
			obj.type = elm.name;
			obj.src = elm.attribs.url;
		}
		else if (elm.name == 'meta') {
			/*
			//youtube
			if (elm.attribs.property.includes('image')) {
				obj.type = 'img';
				obj.src = elm.attribs.content;
			}
			*/
		}
		else if (elm.name == 'link') {
			obj.type = (elm.attribs.type) ? elm.attribs.type : 'text/css';
			obj.src = elm.attribs.href;
		}
		else if (elm.name == 'script') {
			obj.type = (elm.attribs.type) ? elm.attribs.type : 'text/javascript';
			obj.src = elm.attribs.src;
		}
		else {
			resolve();
			return;			
		}

		obj.host = urlapi.parse(obj.src).hostname;

		if (!obj.host) {
			obj.host = urlapi.parse(_URL).hostname;			
		}

		arr.push(obj);

		resolve();
	});
}

function _trySetChildsAsync (arr, elm) {
	return new Promise((resolve) => {
		var children = [];

		if (elm.children) {
			children = elm.children
			.filter(x => x.name)
			.map(x => x.name);
		}

		arr.push({ 
			'tag': elm.name,
			'child': {
				'count': children.length,
				'tags': children }
			});

		resolve();
	});
}

function _trySetTreeAsync (arr, elm, depth) {
	return new Promise((resolve) => {
		arr.push({ 
			'tag': elm.name,
			'depth': depth 
		});

		resolve();
	});
}

function _doWork (html, resolve, reject) {
	try {
		var result = { 
			'tags': [], 
			'attribs': [],
			'rsxs': [], 
			'childs': [],
			'tree': [] 
		};

		var dom = cheerio.load(html);

		/* rsx imgs
		dom("img").each(function(i, image) {
	        results.push(url.resolve(page_url, $(image).attr('src')));
	    });
	    */

	    _inspectHtml(dom.root().toArray(), 
	    	function (elm, depth) {
	    		Promise.all(
	    			_trySetTagAsync(result.tags, elm).then(
	    				_trySetAttribsAsync(result.attribs, elm)).then(
	    				_trySetRsxsAsync(result.rsxs, elm)).then(
	    				_trySetChildsAsync(result.childs, elm)).then(
	    				_trySetTreeAsync(result.tree, elm, depth))
	    				);
	    	}
	    	);

	    resolve(result);
	} catch (err) {
		reject(err);
	}
}

/**
* Public API
* @API
*/
export class Jscrambler {
	/**
 	* Initialize Jscrambler class
 	* @constructor
 	*/
 	constructor () {
 		_URL = '';
 		_HTML = '';
 	}

	/**
 	* @Set Url
 	* @param {string} value The url of the page to analize.
 	* @returns {Error} if is not a valid url
 	*/
 	setUrl (value) {
 		if (!validUrl.isUri(value)) {
 			return new Error('Invalid url.');
 		}

 		_URL = value;
 	}

	/**
 	* @Set Html
 	* @param {string} value The raw html of the page to analize.
 	*/
 	setHtml (value) {
 		_HTML = value;
 	}

	/**
 	* Get html markup info as async method.
 	* Tip: getInfoAsync().then(ok_method(json)).catch(ko_method(error))
 	* @Get Info Async
 	* @returns {Json} .tags .attribs .rsxs .childs .tree
 	*/
 	getInfoAsync() {
 		return new Promise((resolve, reject) => {
 			if (_HTML == '' && _URL == '') {
 				reject(new Error('You must set url or html string.'));
 			}
 			else if (_URL == '') {
 				_doWork(_HTML, resolve, reject);
 			}
 			else {
 				_getContentAsync(_URL)
 				.then((rawHtml) => _doWork(rawHtml, resolve, reject))
 				.catch((err) => reject(err));
 			}
 		});
 	}
 }