﻿'use strict';

//https://github.com/node-js-libs/cli

const lib = require('./dist/jscrambler');
const cmdLineArgs = require('command-line-args');
const fs = require('fs');

const args = cmdLineArgs([
	{ name: 'url', alias: 'u', type: String, defaultOption: true },
	{ name: 'html', alias: 's', type: String }
	]);

const js = new lib.Jscrambler();

if (args.url)
	js.setUrl(args.url);

if (args.html)
	js.setHtml(args.html);

js.getInfoAsync()
	.then((json) => { 
		fs.writeFile('result.json', JSON.stringify(json, null, 2));
		
		console.log(json);
		//console.log(JSON.stringify(json, null, 1))
	})
	.catch((err) => console.error(err));

/*
exports = module.exports = require('./src/jscrambler');
exports.version = require('./package.json').version;
*/

/*
module.exports = require('./dist/jscrambler');
*/